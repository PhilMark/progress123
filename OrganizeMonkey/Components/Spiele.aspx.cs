﻿using Administration;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OrganizeMonkey.Components
{
    /// <summary>
    /// Logik für Quizseite
    /// </summary>
    public partial class Spiele : System.Web.UI.Page
    {
        public string answer1;
        public string answer2;
        public string answer3;
        public string answer4;
        public string url2;
        public string correctAnswer2;
        public string question2;

        /// <summary>
        /// Seite Laden
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            QuizEdit.Visible = Request.QueryString["isAdmin"] != null;
            LoadQuestion();
        }

        /// <summary>
        /// BIndet Values an die jeweiligen COntrolls
        /// </summary>
        protected void LoadQuestion()
        {
            Quiz quiz = new Quiz();
            string[] quizArray = quiz.GetQuestion(Convert.ToInt32(Session["userId"]));
            if (quizArray.Length > 0)
            {
                answer1 = quizArray[1];
                answer2 = quizArray[2];
                answer3 = quizArray[3];
                answer4 = quizArray[4];
                question2 = quizArray[0];
                correctAnswer2 = quizArray[5];

                ImageQuiz.ImageUrl = quizArray[6];
                ImageQuiz.Visible = ImageQuiz.ImageUrl.Length > 0;
            }
            else
            {
                answer1 = "";
                answer2 = "";
                answer3 = "";
                answer4 = "";
                question2 = "";
                correctAnswer2 = "";
                ImageQuiz.ImageUrl = "";
                ImageQuiz.Visible = ImageQuiz.ImageUrl.Length > 0;
            }
        }

        /// <summary>
        /// Neue Frage anlegen
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event</param>
        protected void QuizInsert_Click(object sender, EventArgs e)
        {
            int? documentId = null;
            if (PicUpload.HasFile)
            {
                Guid guid = Guid.NewGuid();
                try
                {
                    string filename = Path.GetFileName(guid + System.IO.Path.GetExtension(this.PicUpload.PostedFile.FileName));
                    PicUpload.SaveAs(Server.MapPath("~/Files/") + filename);

                    Document document = new Document();
                    documentId = document.InsertDocument(filename, Page.ResolveUrl("~/Files/"));
                }
                catch (Exception ex)
                {
                    //StatusLabel.Text = "Upload status: The file could not be uploaded. The following error occured: " + ex.Message;
                }
            }

            Quiz quiz = new Quiz();
            quiz.InsertQuiz(Convert.ToInt32(Session["userId"]), Question.Text, TextBox1.Text, TextBox2.Text, TextBox3.Text, TextBox4.Text, DropDOwnAnswer.SelectedValue, documentId);
            questionInsert.Visible = true;
        }
    }
}