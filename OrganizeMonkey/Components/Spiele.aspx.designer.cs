﻿//------------------------------------------------------------------------------
// <automatisch generiert>
//     Dieser Code wurde von einem Tool generiert.
//
//     Änderungen an dieser Datei können fehlerhaftes Verhalten verursachen und gehen verloren, wenn
//     der Code neu generiert wird.
// </automatisch generiert>
//------------------------------------------------------------------------------

namespace OrganizeMonkey.Components {
    
    
    public partial class Spiele {
        
        /// <summary>
        /// QuizEdit-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Zum Ändern Felddeklaration aus der Designerdatei in eine Code-Behind-Datei verschieben.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder QuizEdit;
        
        /// <summary>
        /// Question-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Zum Ändern Felddeklaration aus der Designerdatei in eine Code-Behind-Datei verschieben.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox Question;
        
        /// <summary>
        /// DropDOwnAnswer-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Zum Ändern Felddeklaration aus der Designerdatei in eine Code-Behind-Datei verschieben.
        /// </remarks>
        protected global::System.Web.UI.WebControls.DropDownList DropDOwnAnswer;
        
        /// <summary>
        /// PicUpload-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Zum Ändern Felddeklaration aus der Designerdatei in eine Code-Behind-Datei verschieben.
        /// </remarks>
        protected global::System.Web.UI.WebControls.FileUpload PicUpload;
        
        /// <summary>
        /// TextBox1-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Zum Ändern Felddeklaration aus der Designerdatei in eine Code-Behind-Datei verschieben.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox TextBox1;
        
        /// <summary>
        /// TextBox2-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Zum Ändern Felddeklaration aus der Designerdatei in eine Code-Behind-Datei verschieben.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox TextBox2;
        
        /// <summary>
        /// TextBox3-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Zum Ändern Felddeklaration aus der Designerdatei in eine Code-Behind-Datei verschieben.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox TextBox3;
        
        /// <summary>
        /// TextBox4-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Zum Ändern Felddeklaration aus der Designerdatei in eine Code-Behind-Datei verschieben.
        /// </remarks>
        protected global::System.Web.UI.WebControls.TextBox TextBox4;
        
        /// <summary>
        /// questionInsert-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Zum Ändern Felddeklaration aus der Designerdatei in eine Code-Behind-Datei verschieben.
        /// </remarks>
        protected global::System.Web.UI.WebControls.PlaceHolder questionInsert;
        
        /// <summary>
        /// ImageQuiz-Steuerelement
        /// </summary>
        /// <remarks>
        /// Automatisch generiertes Feld
        /// Zum Ändern Felddeklaration aus der Designerdatei in eine Code-Behind-Datei verschieben.
        /// </remarks>
        protected global::System.Web.UI.WebControls.Image ImageQuiz;
    }
}
