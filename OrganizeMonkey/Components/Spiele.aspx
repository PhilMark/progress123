﻿<%@ Page Title="" Language="C#" MasterPageFile="~/OrganizeMonkey.Master" AutoEventWireup="true" CodeBehind="Spiele.aspx.cs" Inherits="OrganizeMonkey.Components.Spiele" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="default green row">
        <div class="col-12">
            <asp:PlaceHolder runat="server" ID="QuizEdit">
                <div class="row" style="background-color: floralwhite; padding-bottom: 20px; background-color: floralwhite; padding-bottom: 20px; padding-top: 20px;">
                    <div class="col-6">
                        <asp:TextBox runat="server" ID="Question" class="form-control form-control-sm" placeholder="Frage" required />
                    </div>
                    <div class="col-2">
                        <asp:DropDownList ID="DropDOwnAnswer" runat="server" CssClass="form-control">
                            <asp:ListItem Value="1">Korrekte Antwort: 1</asp:ListItem>
                            <asp:ListItem Value="2">Korrekte Antwort: 2</asp:ListItem>
                            <asp:ListItem Value="3">Korrekte Antwort: 3</asp:ListItem>
                            <asp:ListItem Value="4">Korrekte Antwort: 4</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-2">
                        <asp:FileUpload ID="PicUpload" runat="server" />
                    </div>
                    <div class="col-2">
                        <asp:Button class="btn btn-primary" type="submit" runat="server" OnClick="QuizInsert_Click" Text="Frage anlegen" />
                    </div>
                </div>
                <div class="row" style="background-color: floralwhite; padding-bottom: 20px;">
                    <div class="col">
                        <asp:TextBox runat="server" ID="TextBox1" class="form-control form-control-sm" placeholder="Antwort 1" required />
                    </div>
                    <div class="col">
                        <asp:TextBox runat="server" ID="TextBox2" class="form-control form-control-sm" placeholder="Antwort 2" required />
                    </div>
                    <div class="col">
                        <asp:TextBox runat="server" ID="TextBox3" class="form-control form-control-sm" placeholder="Antwort 3" required />
                    </div>
                    <div class="col">
                        <asp:TextBox runat="server" ID="TextBox4" class="form-control form-control-sm" placeholder="Antwort 4" required />
                    </div>
                </div>
            </asp:PlaceHolder>
            <div class="row">
                <script>
                    $(document).ready(function () {
                        function checkAnswer(p1, sender) {
                            $("#quizbutton1").prop('disabled', true);
                            $("#quizbutton2").prop('disabled', true);
                            $("#quizbutton3").prop('disabled', true);
                            $("#quizbutton4").prop('disabled', true);
                            if (p1 === $("#correctAnswer").text()) {
                                $(sender).css('background-color', 'green');
                            }
                            else {
                                $(sender).css('background-color', 'red');
                            }
                            setTimeout(function () {
                                location.reload();
                            }, 2000);
                        }
                        $("#quizbutton1").click(function () { checkAnswer("'1'", this); });
                        $("#quizbutton2").click(function () { checkAnswer("'2'", this); });
                        $("#quizbutton3").click(function () { checkAnswer("'3'", this); });
                        $("#quizbutton4").click(function () { checkAnswer("'4'", this); });

                    });
                </script>
                <div class="kachel col">
                    <asp:PlaceHolder ID="questionInsert" runat="server" Visible="false">
                        <div class="bg-success text-white text-center kachel">
                            <h3 style="text-align: center">Frage angelegt!</h3>
                        </div>
                    </asp:PlaceHolder>
                    <div class="quiz">
                        <div class="question">
                            <span><%= question2 %></span>
                            <div class="img">
                                <asp:Image ID="ImageQuiz" CssClass="img-border" runat="server" Visible="false" />
                            </div>
                        </div>
                        <div>
                            <input class="quizbutton btn img-border" id="quizbutton1" type="button" value="<%= answer1 %>" />
                            <input class="quizbutton btn img-border" id="quizbutton2" type="button" value="<%= answer2 %>" />
                            <input class="quizbutton btn img-border" id="quizbutton3" type="button" value="<%= answer3 %>" />
                            <input class="quizbutton btn img-border" id="quizbutton4" type="button" value="<%= answer4 %>" />
                            <label id="correctAnswer" style="display: none;">'<%= correctAnswer2 %>'</label>
                            <br />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
