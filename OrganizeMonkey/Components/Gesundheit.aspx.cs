﻿using Administration;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OrganizeMonkey.Components
{   
    /// <summary>
    /// Logik für Dokumente
    /// </summary>
    public partial class Gesundheit : System.Web.UI.Page
    {
        /// <summary>
        /// Seite laden
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            QuizEdit.Visible = Request.QueryString["isAdmin"] != null;
            GetDocuments();
        }

        /// <summary>
        /// Dokument Upload
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event</param>
        protected void Upload_Click(object sender, EventArgs e)
        {
            int? documentId = null;
            if (DocUpload.HasFile)
            {
                Guid guid = Guid.NewGuid();
                try
                {
                    string filenameOriginal = this.DocUpload.PostedFile.FileName;
                    string filename = Path.GetFileName(guid + System.IO.Path.GetExtension(this.DocUpload.PostedFile.FileName));
                    DocUpload.SaveAs(Server.MapPath("~/Files/") + filename);

                    Document document = new Document();
                    documentId = document.InsertDocument(Convert.ToInt32(Session["userId"]), Page.ResolveUrl("~/Files/") + filename, filenameOriginal);
                    GetDocuments();
                }
                catch (Exception ex)
                {
                    //ToDo: StatusLabel.Text = ex.Message;
                }
            }
        }

        /// <summary>
        /// Hole alle Dokumente und schreibe in Repeater
        /// </summary>
        private void GetDocuments()
        {
            Document documents = new Document();

            DataTable images = documents.GetDocuments(Convert.ToInt32(Session["userId"]));

            repeaterImages.DataSource = images;
            repeaterImages.DataBind();
        }
    }
}