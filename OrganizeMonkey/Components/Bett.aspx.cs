﻿using Administration;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
/// <summary>
/// Logik der Komponenten
/// </summary>
namespace OrganizeMonkey.Components
{
    /// <summary>
    /// Logik für Gallerie
    /// </summary>
    public partial class Bett : System.Web.UI.Page
    {
        /// <summary>
        /// Seite laden
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            QuizEdit.Visible = Request.QueryString["isAdmin"] != null;
            GetImages();
        }

        /// <summary>
        /// Upload eines Bildes
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event</param>
        protected void Upload_Click(object sender, EventArgs e)
        {
            int? documentId = null;
            if (PicUpload.HasFile)
            {
                Guid guid = Guid.NewGuid();
                try
                {
                    string filename = Path.GetFileName(guid + System.IO.Path.GetExtension(this.PicUpload.PostedFile.FileName));
                    PicUpload.SaveAs(Server.MapPath("~/Files/") + filename);

                    Document document = new Document();
                    documentId = document.InsertDocumentImage(Convert.ToInt32(Session["userId"]), Page.ResolveUrl("~/Files/") + filename);
                    GetImages();
                }
                catch (Exception ex)
                {
                    //ToDo: StatusLabel.Text = ex.Message;
                }
            }
        }

        /// <summary>
        /// Schreibt Bilder in Repeater
        /// </summary>
        private void GetImages()
        {
            Document documents = new Document();

            DataTable images = documents.GetImages(Convert.ToInt32(Session["userId"]));

            repeaterImages.DataSource = images;
            repeaterImages.DataBind();
        }
    }
}