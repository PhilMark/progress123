﻿using Administration;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace OrganizeMonkey.Components
{
    /// <summary>
    /// Der <see cref="OrganizeMonkey.Components"/>-Namespace hält Klassen für die Implementation der eigentlichen ASP-Unterseiten mit MasterPage-Funktion
    /// </summary>
    [System.Runtime.CompilerServices.CompilerGenerated]
    class NamespaceDoc
    {
    }

    /// <summary>
    /// Logik für Kontaktseite
    /// </summary>
    public partial class Hilfe : System.Web.UI.Page
    {
        /// <summary>
        /// Laden der Seite
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            ContactEdit.Visible = Request.QueryString["isAdmin"] != null;

            if (Session["userId"] == null)
            {
                Session["userId"] = "1";
            }

            if (!Page.IsPostBack)
            {
                GetContactList();
                MailText.Text = GetMailText();
            }
        }

        /// <summary>
        /// Holt den Mailtext als string
        /// </summary>
        /// <returns>Mailtext</returns>
        private string GetMailText()
        {
            Administration.User user = new Administration.User();
            string userId = Session["userId"].ToString();
            string mailText = user.GetUserContactTextById(Convert.ToInt32(userId));
            return mailText;
        }

        /// <summary>
        /// Legt einen neuen Kontakt an
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event</param>
        protected void Contact_Click(object sender, EventArgs e)
        {
            int? documentId = null;
            if (PicUpload.HasFile)
            {
                Guid guid = Guid.NewGuid();
                try
                {
                    string filename = Path.GetFileName(guid + System.IO.Path.GetExtension(this.PicUpload.PostedFile.FileName));
                    PicUpload.SaveAs(Server.MapPath("~/Files/") + filename);

                    Document document = new Document();
                    documentId = document.InsertDocument(filename, Page.ResolveUrl("~/Files/"));
                }
                catch (Exception ex)
                {
                    //StatusLabel.Text = ex.Message;
                }
            }

            Contact contact = new Contact();
            int i = contact.SaveContact(Convert.ToInt32(Session["userId"]), ContactName.Text, ContactAdress.Text, documentId);
            contactInsert.Visible = true;
            GetContactList();
        }

        /// <summary>
        /// BIndet Kontakte an Repeater
        /// </summary>
        protected void GetContactList()
        {
            Administration.Contact contact = new Administration.Contact();
            DataTable table = contact.GetContacts(Convert.ToInt32(Session["userId"]));

            repeaterContacts.DataSource = table;
            repeaterContacts.DataBind();
        }

        /// <summary>
        /// Sendet Mail
        /// </summary>
        /// <param name="sender">ist die Mailadresse</param>
        /// <param name="e">event</param>
        protected void SendMail_Click(object sender, EventArgs e)
        {
            string mailAdress = (sender as LinkButton).CommandArgument.ToString();

            Administration.User user = new Administration.User();
            string userId = Session["userId"].ToString();
            string name = user.GetUserNameById(Convert.ToInt32(userId));
            string mailText = user.GetUserContactTextById(Convert.ToInt32(userId));

            Message message = new Message();
            message.SendMail(mailAdress, GetMailText(), name);
            mailSent.Visible = true;
        }

        /// <summary>
        /// Speichert neuen MailText
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event</param>
        protected void Mailtext_Click(object sender, EventArgs e)
        {
            Administration.User user = new Administration.User();
            string userId = Session["userId"].ToString();
            user.SetUserMailtextById(Convert.ToInt32(userId), MailText.Text);
            
            MailText.Text = GetMailText();
        }
    }
}