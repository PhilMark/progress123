﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SpotifyAPI;
using SpotifyAPI.Web;
using SpotifyAPI.Web.Auth;
using SpotifyAPI.Web.Models;
using SpotifyAPI.Web.Enums;
using System.IO;
using System.Net;
using System.Data;
using System.Web.UI.HtmlControls;
 

namespace OrganizeMonkey
{
    /// <summary>
    /// Logik für die Masterpage
    /// </summary>
    public partial class OrganizeMonkey : System.Web.UI.MasterPage
    {
        public int adminId = 1;
        public int userTypeId = 1;
        /// <summary>
        /// Seite laden
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            UserSelect.Visible = Request.QueryString["isAdmin"] != null; ;
            
            if (!Page.IsPostBack)
            {
                SetUserName();
                GetUserList();
            }            
        }

        /// <summary>
        /// Setzt den Nutzernamen für Adminansicht
        /// </summary>
        private void SetUserName()
        {
            Administration.User user = new Administration.User();

            if (Session["userId"] == null)
            {
                LabelName.Text = user.GetFirstUserName();
                Session["userId"] = "1";
            }
            else
            {
                string userId = Session["userId"].ToString();
                LabelName.Text = user.GetUserNameById(Convert.ToInt32(userId));
            }          
        }

        /// <summary>
        /// Baut die dynamische URL auf
        /// </summary>
        /// <param name="url">URL</param>
        /// <returns>Gibt URL zurück</returns>
        protected string ResolveUrlTmp(string url)
        {
            string result = ResolveUrl(url);
            result += Request.QueryString["isAdmin"] != null ? "?isAdmin=true" : "";
            return result;
        }

        /// <summary>
        /// Erstellen eines Users
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event</param>
        protected void CreateUser(object sender, EventArgs e)
        {
            Administration.User user = new Administration.User();

            user.InsertUser(firstname.Text, lastname.Text, 1, 1);
            GetUserList();
        }

        /// <summary>
        /// Wechsel des Nutzers durch DropDown 
        /// </summary>
        /// <param name="source">source</param>
        /// <param name="e">User-Id</param>
        protected void RepeaterUser_ItemCommand(object source, RepeaterCommandEventArgs e)
        {
            int userId = Convert.ToInt32(e.CommandName);
            Session["userId"] = userId;
            Response.Redirect(ResolveUrlTmp("~/default.aspx"));
        }

        /// <summary>
        /// Setzt alle User in eine Dropdown
        /// </summary>
        protected void GetUserList()
        {
            Administration.User user = new Administration.User();
            DataTable table = user.GetUsers();

            repeaterUsers.DataSource = table;
            repeaterUsers.DataBind();
        }
    }
}
    
   
