﻿<%@ Page Language="C#" EnableSessionState="true" AutoEventWireup="true" MasterPageFile="~/OrganizeMonkey.Master" CodeBehind="default.aspx.cs" Inherits="OrganizeMonkey._default" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">

    <div class="default magenta row">
        <div class="col-6">
            <div class="kachel todo">
                <h1>zu erledigen</h1>
                <div class="row">
                    <asp:PlaceHolder ID="scheduleInsert" runat="server" Visible="false">
                        <div class="bg-success text-white text-center kachel">
                            <h3 style="text-align: center">Kontakt angelegt!</h3>
                        </div>
                    </asp:PlaceHolder>
                    <asp:PlaceHolder ID="ScheduleEdit" runat="server">
                        <div class="col">
                            <asp:TextBox runat="server" ID="scheduleName" class="form-control form-control-sm" placeholder="Termin anlegen" />
                        </div>
                        <div class="col">
                            <asp:TextBox TextMode="DateTimeLocal" runat="server" class="form-control test1" ID='datetimepicker2'></asp:TextBox>
                        </div>
                        <div class="col">
                            <asp:Button class="btn btn-primary" type="submit" runat="server" OnClick="InsertSchedule_Click" Text="Termin anlegen" />
                        </div>
                        <!--dirty! ToDo: Styling-->
                        <br />
                        <br />
                        <br />
                        <br />
                    </asp:PlaceHolder>
                </div>
                <asp:CheckBoxList ID="chkSchedules" runat="server" CssClass="tableSchedules"
                    OnSelectedIndexChanged="chkSchedules_SelectedIndexChanged" AutoPostBack="true">
                </asp:CheckBoxList>
            </div>
        </div>
        <div class="col-6" style="pointer-events: none;">
            <div class="kachel weather">
                <div class="row">
                    <div class="col-3"></div>
                    <div id="wcom-ab92de27fc33bb6e270758ca7904e346" style="height: 280px !important; max-height: 280px!important;" class="wcom-default w300x250 col-6">
                        <link rel="stylesheet" href="//cs3.wettercomassets.com/woys/5/css/w.css" media="all">
                        <a style="color: #ffffff; font-weight: bold; font-size: 60px; text-shadow: 2px 2px rgba(0, 0, 0, 0.3);"
                            href="https://www.wetter.com/deutschland/berlin/DE0001020.html" target="_blank" rel="nofollow" title="Wetter Berlin">Wetter</a><div id="wcom-ab92de27fc33bb6e270758ca7904e346-weather"></div>
                        <script type="text/javascript" src="//cs3.wettercomassets.com/woys/5/js/w.js"></script>
                        <script type="text/javascript">_wcomWidget({ id: 'wcom-ab92de27fc33bb6e270758ca7904e346', location: 'DE0001020', format: '300x250', type: 'spaces' });</script>
                        <script type="text/javascript">
                            $(function () {
                                $('.test1').attr('style', 'display: block!important;');
                            });
                        </script>
                    </div>
                    <div class="col-3"></div>
                </div>
            </div>
            <div class="kachel">
                <h1>Nachrichten</h1>
                <!-- start feedwind code -->
                <script type="text/javascript" src="https://feed.mikle.com/js/fw-loader.js" data-fw-param="99717/"></script>
                <!-- end feedwind code -->
            </div>
        </div>
    </div>
</asp:Content>
