﻿<%@ Page Title="" Language="C#" MasterPageFile="~/OrganizeMonkey.Master" AutoEventWireup="true" CodeBehind="Hilfe.aspx.cs" Inherits="OrganizeMonkey.Components.Hilfe" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="default blue">
        <asp:PlaceHolder runat="server" ID="ContactEdit">
            <div class="row" style="background-color: floralwhite; padding-bottom: 20px; padding-top: 20px;">
                <div class="col">
                    <asp:TextBox runat="server" ID="ContactName" class="form-control form-control-sm" placeholder="Name" />
                </div>
                <div class="col">
                    <asp:TextBox runat="server" type="email" ID="ContactAdress" class="form-control form-control-sm" placeholder="Mailadresse" />
                </div>
                <div class="col">
                    <asp:FileUpload ID="PicUpload" runat="server" />
                </div>
                <div class="col">
                    <asp:Button class="btn btn-primary" type="submit" runat="server" OnClick="Contact_Click" Text="Kontakt anlegen" />
                </div>
            </div>
            <div class="row" style="background-color: floralwhite; padding-bottom: 20px;">
                <div class="col-4"></div>
                <div class="col-4">
                    <asp:TextBox runat="server" placeholder="Mailtext" class="form-control" Style="resize: none" Rows="10" Width="100%" TextMode="MultiLine" ID="MailText" />
                </div>
                <div class="col-1"></div>
                <div class="col-3">
                    <asp:Button class="btn btn-primary" type="submit" runat="server" ID="Button1" OnClick="Mailtext_Click" Text="Mailtext ändern" />
                </div>
            </div>

        </asp:PlaceHolder>
        <div class="row">
            <div class="col-3"></div>
            <div class="col-6">
                <asp:PlaceHolder ID="mailSent" runat="server" Visible="false">
                    <div class="bg-success text-white text-center kachel">
                        <h3 style="text-align: center">Mail abgeschickt!</h3>
                    </div>
                </asp:PlaceHolder>
                <asp:PlaceHolder ID="contactInsert" runat="server" Visible="false">
                    <div class="bg-success text-white text-center kachel">
                        <h3 style="text-align: center">Kontakt angelegt!</h3>
                    </div>
                </asp:PlaceHolder>
                <div class="kachel kontakt">
                    <h1>Kontakte</h1>
                    <ul>
                        <asp:Repeater ID="repeaterContacts" runat="server">
                            <ItemTemplate>
                                <li>

                                    <asp:Image runat="server" ImageUrl='<%# Eval("url") %>' />

                                    <asp:LinkButton OnClick="SendMail_Click" CommandName='<%# Eval("mail") %>' CommandArgument='<%# Eval("mail") %>' runat="server" Text='<%# Eval("name") %>' />
                                    <asp:HiddenField runat="server" Value='<%# Eval("mail") %>' />
                                </li>
                            </ItemTemplate>
                        </asp:Repeater>
                    </ul>
                </div>
            </div>
            <div class="col-3"></div>
        </div>
    </div>
</asp:Content>
