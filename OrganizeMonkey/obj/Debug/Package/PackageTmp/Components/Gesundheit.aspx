﻿<%@ Page Title="" Language="C#" MasterPageFile="~/OrganizeMonkey.Master" AutoEventWireup="true" CodeBehind="Gesundheit.aspx.cs" Inherits="OrganizeMonkey.Components.Gesundheit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="default row red">
        <asp:PlaceHolder runat="server" ID="QuizEdit">
            <div class="row col-12" style="background-color: floralwhite; padding-bottom: 20px; padding-top: 20px; margin-left: 0px!important">
                <div class="col-2"></div>
                <div class="col-4">
                    <asp:FileUpload ID="DocUpload" runat="server" />
                </div>
                <div class="col-4">
                    <asp:Button class="btn btn-primary" type="submit" runat="server" OnClick="Upload_Click" Text="Dokument speichern" />
                </div>
                <div class="col-2"></div>
            </div>
        </asp:PlaceHolder>
        <div class="col div_full gallery cf kachel">
            <h1>Dokumente</h1>
            <ul>
                <asp:Repeater ID="repeaterImages" runat="server">

                    <ItemTemplate>
                        <li>
                            <a href='<%# Eval("url") %>'><%# Eval("name") %></a>
                        </li>
                    </ItemTemplate>

                </asp:Repeater>
            </ul>
        </div>
    </div>


</asp:Content>
