﻿<%@ Page Title="" Language="C#" MasterPageFile="~/OrganizeMonkey.Master" AutoEventWireup="true" CodeBehind="Bett.aspx.cs" Inherits="OrganizeMonkey.Components.Bett" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderMain" runat="server">
    <div class="default row yellow">
        <asp:PlaceHolder runat="server" ID="QuizEdit">

            <div class="row col-12" style="background-color: floralwhite; background-color: floralwhite; padding-bottom: 20px; padding-top: 20px; margin-left: 0px!important">
                <div class="col-2"></div>
                <div class="col-4">
                    <asp:FileUpload ID="PicUpload" runat="server" />
                </div>
                <div class="col-4">
                    <asp:Button class="btn btn-primary" type="submit" runat="server" OnClick="Upload_Click" Text="Bild speichern" />
                </div>
                <div class="col-2"></div>
            </div>
        </asp:PlaceHolder>
        <div class="col gallery cf kachel">
            <h1>Fotos</h1>
            <asp:Repeater ID="repeaterImages" runat="server">
                <ItemTemplate>
                    <div class="col-3">
                        <asp:Image runat="server" CssClass="img-border" ImageUrl='<%# Eval("url") %>' Width="250px" />
                    </div>
                </ItemTemplate>
            </asp:Repeater>
        </div>
    </div>
</asp:Content>
