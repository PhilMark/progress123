﻿# Progress ![](/styles/Unbenannt.png)

---
## 1. Setup
>Software aufrufen unter:
www.progress.organize-monkey.com

---
## 2. Inbetriebnahme
### 2.1. User-Ansicht
>#### 2.1.1. Heute
>>In dieser Ansicht kann der Patient seine heutigen Aufgaben/Termine einsehen und als erledigt kennzeichnen.
>#### 2.1.2. Kontakte
>>Mit einem Klick auf den jeweiligen Kontakt wird an diesen (unter der jeweilig abgespeichten eMail-Adresse) eine Mail versandt.
>#### 2.1.3. Quiz
>>Hier kann der User für ihn hinterlegte Quizfragen beantworten.
>#### 2.1.4. Gallerie
>>Der Nutzer hat hier eine Gallerie-Funktion für alle Bilder, welche für Ihn hinterlegt wurden.
>#### 2.1.5. Dokumente 
>>Der Nutzer hat hier eine Download-Funktion für alle Dokumente, welche für Ihn hinterlegt wurden.
---
### 2.2. Verwaltungsansicht
>Die Verwaltungsansicht ist jederzeit zu erreichen, indem hinter die jeweilige URL der Zusatz "?isAdmin=true" (z.B.: http://www.progress.organize-monkey.com/?isAdmin=true) gesetzt wird.
>Somit kann der Nutzer, welcher bearbeitet wird jederzeit gewählt werden. Zudem kann hier ein neuer Nutzer angelegt werden.
>#### 2.2.1. Heute
>>Ein Administrator kann hier Termine/Aufgaben für den jeweiligen Patienten anlegen.
>#### 2.2.2. Kontakte
>>Berechtigte User (Familie oder Personal) können hier Kontakte für den gewählten User anlegen. Zudem ist es möglich den Mail-Text anzupassen.
>#### 2.2.3. Quiz
>>Als Administrator können hier Quizfragen für den Patienten angelegt werden. Dies können "Allgemeinfragen" sein sowie auch Fragen zu Verwandten und Freunden.
>#### 2.2.4. Gallerie
>>Hier ist ein Upload von Bildern für den Patienten möglich.
>#### 2.2.5. Dokumente
>>Hier ist ein Upload von Dokumenten für den Patienten möglich.
---
### 3. Technische Dokumentation
>Die technische Dokumentation ist erreichbar unter:
>www.progress.organize-monkey.com/help/index.aspx

>oder als Download unter:
>www.progress.organize-monkey.com/help/Documentation.chm

