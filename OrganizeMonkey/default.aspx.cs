﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using SpotifyAPI.Web;
using System.Data;

namespace OrganizeMonkey
{
    /// <summary>
    /// Der <see cref="OrganizeMonkey"/>-Namespace hält Klassen für die Implementation der eigentlichen ASP-Website mit MasterPage-Funktion
    /// </summary>
    [System.Runtime.CompilerServices.CompilerGenerated]
    class NamespaceDoc
    {
    }

    /// <summary>
    /// Logik für Startseite
    /// </summary>
    public partial class _default : System.Web.UI.Page
    {       
        /// <summary>
        /// Start
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event</param>
        protected void Page_Load(object sender, EventArgs e)
        {
            ScheduleEdit.Visible = Request.QueryString["isAdmin"] != null;

            if (Session["userId"] == null)
            {
                Session["userId"] = "1";
            }

            if (!Page.IsPostBack)
            {
                GetScheduleList();
            }
        }

        /// <summary>
        /// Legt einen Termin an
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event</param>
        protected void InsertSchedule_Click(object sender, EventArgs e)
        {
            Administration.Schedule schedule = new Administration.Schedule();

            int i = schedule.InsertSchedule((Convert.ToDateTime(datetimepicker2.Text).ToString("dd.MM.yyyy HH:mm")), Convert.ToInt32(Session["userId"]), scheduleName.Text);
            scheduleInsert.Visible = true;
            GetScheduleList();
        }

        /// <summary>
        /// Holt die Terminliste und schreibt in Repeater
        /// </summary>
        protected void GetScheduleList()
        {
            Administration.Schedule schedule = new Administration.Schedule();
            DataTable table = schedule.GetTodaysSchedules(Convert.ToInt32(Session["userId"]));

            chkSchedules.DataSource = table;
            chkSchedules.DataTextField = "name";
            chkSchedules.DataValueField = "id";
            chkSchedules.DataBind();

            foreach (ListItem item in chkSchedules.Items)
                item.Selected = Convert.ToBoolean(table.Select("id = '" + item.Value +"'")[0]["done"]);
        }

        /// <summary>
        /// Zusammenhang Label und CHeckbox
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event</param>
        protected void repeaterSchedules_ItemDataBound(object sender, RepeaterItemEventArgs e)
        {
            var checkb = e.Item.FindControl("checkboxSchedule");
            checkb.ClientIDMode = ClientIDMode.Static;
            checkb.ID = "checkb_" + (((Label)e.Item.FindControl("labelId")).Text);
        }

        /// <summary>
        /// Updated Termin
        /// </summary>
        /// <param name="sender">sender</param>
        /// <param name="e">event</param>
        protected void chkSchedules_SelectedIndexChanged(object sender, EventArgs e)
        {
            Administration.Schedule schedule = new Administration.Schedule();
            foreach (ListItem item in chkSchedules.Items)
            {                
                schedule.UpdateSchedule(Convert.ToInt32(item.Value), item.Selected);
            }
        }
    }
}