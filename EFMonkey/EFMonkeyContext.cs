﻿using System;
using System.Data.Entity;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration.Conventions;
/// <summary>
/// Entities für den Kontext
/// </summary>
namespace EFMonkey
{
    /// <summary>
    /// Der <see cref="EFMonkey"/>-Namespace hält Klassen für die Implementation des Entity-Frameworks. Nutzung von EF-Code-First
    /// </summary>
    [System.Runtime.CompilerServices.CompilerGenerated]
    class NamespaceDoc
    {
    }

    /// <summary>
    /// Aufbau der Code-First-Datenbank
    /// </summary>
    public class EFMonkeyInitializer : DropCreateDatabaseIfModelChanges<EFMonkeyContext>
    {
        /// <summary>
        /// Initiale Werte setzebn
        /// </summary>
        /// <param name="context">DB context</param>
        protected override void Seed(EFMonkeyContext context)
        {
            IList<UserType> defaultUserTypes = new List<UserType>();

            defaultUserTypes.Add(new UserType() { Name = "Patient" });
            defaultUserTypes.Add(new UserType() { Name = "Pflege" });
            defaultUserTypes.Add(new UserType() { Name = "Familie" });

            foreach (UserType tpr in defaultUserTypes)
                context.UserType.Add(tpr);

            base.Seed(context);
        }
    }

    /// <summary>
    /// Datenbankverbindung und Updateregeln festlegen
    /// </summary>
    public class EFMonkeyContext : DbContext
    {
        /// <summary>
        /// Datenbankverbindung festlegen
        /// </summary>
        public EFMonkeyContext()
        {
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

            builder.DataSource = "db770856813.hosting-data.io";
            builder.UserID = "dbo770856813";
            builder.Password = "castro59";
            builder.InitialCatalog = "db770856813";

            //builder.DataSource = "DESKTOP-KU3S3Q9\\NEU";
            //builder.UserID = "DESKTOP-KU3S3Q9\\PhilM";
            //builder.Password = "";
            //builder.IntegratedSecurity = true;
            //builder.InitialCatalog = "db757812794";

            this.Database.Connection.ConnectionString = builder.ConnectionString;
        }

        /// <summary>
        /// Mappings und Entities festlegen
        /// Fire in the hole!: Legt Datenbank nach Mappings und Entities an
        /// </summary>
        /// <param name="modelBuilder">blublublu</param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {           
            Database.SetInitializer(new EFMonkeyInitializer());
            base.OnModelCreating(modelBuilder);
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            modelBuilder.Configurations.Add(new UserMap());
            modelBuilder.Configurations.Add(new LoginMap());
            modelBuilder.Configurations.Add(new DocumentMap());
            modelBuilder.Configurations.Add(new ContactMap());
            modelBuilder.Configurations.Add(new QuizMap());
            modelBuilder.Configurations.Add(new ScheduleMap());
            modelBuilder.Configurations.Add(new UserTypeMap());
        }
        public DbSet<User> User { get; set; }
        public DbSet<Login> Login { get; set; }
        public DbSet<Document> Document { get; set; }
        public DbSet<Contact> Contact { get; set; }
        public DbSet<Quiz> Quiz { get; set; }
        public DbSet<Schedule> Schedule { get; set; }
        public DbSet<UserType> UserType { get; set; }

        /// <summary>
        /// Save Changes überschreiben für Zeitstempel
        /// </summary>
        /// <returns></returns>
        public override int SaveChanges()
        {
            AddTimestamps();
            return base.SaveChanges();
        }

        /// <summary>
        /// Zeitstempel hinzufügen
        /// </summary>
        private void AddTimestamps()
        { 
            var entities = ChangeTracker.Entries().Where(x => x.Entity is Entity && (x.State == EntityState.Added || x.State == EntityState.Modified));
            
            foreach (var entity in entities)
            {
                if (entity.State == EntityState.Added)
                {
                    ((Entity)entity.Entity).UpdateDate = DateTime.UtcNow;
                    ((Entity)entity.Entity).CreateDate = DateTime.UtcNow;
                }
                else
                {
                    ((Entity)entity.Entity).UpdateDate = DateTime.UtcNow;
                }
            }
        }
    }
}
