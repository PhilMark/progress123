﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/// <summary>
/// Entities für den Kontext
/// </summary>
namespace EFMonkey
{
    /// <summary>
    /// Mappinf des User-Types (not used at the moment)
    /// </summary>
    class UserTypeMap : EntityTypeConfiguration<UserType>
    {
        public UserTypeMap()
        {
            this.HasKey(u => u.Id);
            this.HasMany(p => p.Users).WithRequired(t => t.UserType);
        }
    }

    /// <summary>
    /// Felder für den UserType (not used at the moment)
    /// </summary>
    public class UserType : Entity
    {   
        public string Name { get; set; }
        public IList<User> Users { get; set; }
    }
}
