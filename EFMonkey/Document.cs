﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.ModelConfiguration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/// <summary>
/// Entities für den Kontext
/// </summary>
namespace EFMonkey
{
    /// <summary>
    /// Mapping für Dokumente
    /// </summary>
    class DocumentMap : EntityTypeConfiguration<Document>
    {
        public DocumentMap()
        {
            
        }
    }

    /// <summary>
    /// Felder für Kontakte
    /// </summary>
    public class Document : Entity
    {
        public string Name { get; set; }
        public string Url { get; set; }
        public virtual User DocumentUser { get; set; }
        public virtual User ImageUser { get; set; }
    }
}
