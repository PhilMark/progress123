﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Entities für den Kontext
/// </summary>
namespace EFMonkey
{
    /// <summary>
    /// Mapping für Login (not used at the moment)
    /// </summary>
    class LoginMap : EntityTypeConfiguration<Login>
    {
        public LoginMap()
        {
            this.HasKey(u => u.Id);
            this.HasRequired(u => u.User);
        }
    }


    /// <summary>
    /// Mapping für Login setzen (not used at the moment)
    /// </summary>
    public class Login : Entity
    {   
        public string LoginName { get; set; }
        public string Password { get; set; }
        
        public virtual User User { get; set; }
    }
}
