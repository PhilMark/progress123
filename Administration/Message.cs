﻿using EFMonkey;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
/// <summary>
/// Implementation des Businesslogik
/// </summary>
namespace Administration
{
    /// <summary>
    /// Businesslogik für Emails
    /// </summary>
    public class Message
    {
        /// <summary>
        /// Sendet eine Mail mit vorgefertigtem Text
        /// ToDo:Try-Catch
        /// </summary>
        /// <param name="mailAdress">Mailadresse</param>
        /// <param name="message">Textnachricht</param>
        /// <param name="name">Name des Empfängers</param>
        public void SendMail(string mailAdress, string message, string name)
        {
            
            MailMessage mail = new MailMessage();

            //Setting From , To and CC
            mail.From = new MailAddress("mail@progress.organize-monkey.com", name);
            mail.Subject = name + " möchte mit Ihnen Kontakt aufnehmen";
            mail.To.Add(new MailAddress(mailAdress));
            mail.Body = message;

            //smtpClient.Send(mail);

            using (SmtpClient client = new SmtpClient())
            {
                client.EnableSsl = true;
                client.UseDefaultCredentials = false;
                client.Credentials = new NetworkCredential("mail@progress.organize-monkey.com", "castro59");
                client.Host = "smtp.ionos.de";
                client.Port = 587;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;

                client.Send(mail);
            }            
        }
    }
}
