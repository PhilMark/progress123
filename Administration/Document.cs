﻿
using EFMonkey;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
/// <summary>
    /// Implementation des Businesslogik
    /// </summary>
namespace Administration
{
    /// <summary>
    /// Businesslogik für Dokumente
    /// </summary>
    public class Document
    {
        /// <summary>
        /// Fügt ein Dokument ein
        /// </summary>
        /// <param name="name">Name des Dokumentes</param>
        /// <param name="url">URL des Dokumentes</param>
        /// <returns>Gibt URL des neuen Dokumentes zurück</returns>
        public int InsertDocument(string name, string url)
        {
            using (EFMonkeyContext context = new EFMonkey.EFMonkeyContext())
            {
                EFMonkey.Document document = new EFMonkey.Document();

                document.Name = name;
                document.Url = url;

                context.Document.Add(document);

                context.SaveChanges();

                return document.Id;
            }
        }

        /// <summary>
        /// Fügt ein Profilbild für einen Kontakt ein
        /// </summary>
        /// <param name="userId">User-Id</param>
        /// <param name="url">URL des Bildes</param>
        /// <returns>Gibt URL des neuen Dokumentes zurück</returns>
        public int InsertDocumentImage(int userId, string url)
        {
            using (EFMonkeyContext context = new EFMonkey.EFMonkeyContext())
            {
                EFMonkey.User user = context.User.FirstOrDefault(u => u.Id == userId);

                EFMonkey.Document document = new EFMonkey.Document();

                document.ImageUser = user;
                document.Url = url;

                context.Document.Add(document);

                context.SaveChanges();

                return document.Id;
            }
        }

        /// <summary>
        /// Fügt ein Dokument für den User ein
        /// </summary>
        /// <param name="userId">User-Id</param>
        /// <param name="url">URL-Pfad des Dokuments</param>
        /// <param name="name">Name der Datei</param>
        /// <returns>Gibt URL des neuen Dokumentes zurück</returns>
        public int InsertDocument(int userId, string url, string name)
        {
            using (EFMonkeyContext context = new EFMonkey.EFMonkeyContext())
            {
                EFMonkey.User user = context.User.FirstOrDefault(u => u.Id == userId);

                EFMonkey.Document document = new EFMonkey.Document();

                document.DocumentUser = user;
                document.Url = url;
                document.Name = name;

                context.Document.Add(document);

                context.SaveChanges();

                return document.Id;
            }
        }

        /// <summary>
        /// Gibt deine Tabelle mit Dokumenten für einen User zurück
        /// </summary>
        /// <param name="userId">User-Id</param>
        /// <returns>DataTable mit Daten für Dokumente</returns>
        public DataTable GetDocuments(int userId)
        {
            using (EFMonkeyContext context = new EFMonkey.EFMonkeyContext())
            {
                List<EFMonkey.Document> documents = context.Document.Where(d => d.DocumentUser.Id == userId).ToList();

                DataTable table = new DataTable();
                if (documents.Count > 0)
                {
                    table.Columns.Add("url");
                    table.Columns.Add("name");

                    foreach (EFMonkey.Document document in documents)
                    {
                        DataRow row = table.NewRow();
                        row["url"] = document.Url;
                        row["name"] = document.Name;

                        table.Rows.Add(row);
                        table.AcceptChanges();
                    }
                }
                return table;
            }
        }

        /// <summary>
        /// Gibt deine Tabelle mit Bildern für einen User zurück
        /// </summary>
        /// <param name="userId">User-Id</param>
        /// <returns>DataTable mit Daten für Dokumente</returns>
        public DataTable GetImages(int userId)
        {
            using (EFMonkeyContext context = new EFMonkey.EFMonkeyContext())
            {
                List<EFMonkey.Document> images = context.Document.Where(d => d.ImageUser.Id == userId).ToList();

                DataTable table = new DataTable();
                if (images.Count > 0)
                {
                    table.Columns.Add("url");

                    foreach (EFMonkey.Document image in images)
                    {
                        DataRow row = table.NewRow();
                        row["url"] = image.Url;
                       
                        table.Rows.Add(row);
                        table.AcceptChanges();
                    }
                }
                return table;
            }
        }
    }
}