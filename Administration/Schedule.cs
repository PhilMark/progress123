﻿using EFMonkey;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/// <summary>
/// Implementation des Businesslogik
/// </summary>
namespace Administration
{
    /// <summary>
    /// Businesslogik für Termie
    /// </summary>
    public class Schedule
    {
        /// <summary>
        /// Fügt einen Termin für einen Nutzer ein
        /// </summary>
        /// <param name="date">Datum mit Uhrzeit</param>
        /// <param name="userId">User-Id</param>
        /// <param name="text">Titel des Termins</param>
        /// <returns>Id des neuen Termins</returns>
        public int InsertSchedule(string date, int userId, string text)
        {
            using (EFMonkeyContext context = new EFMonkey.EFMonkeyContext())
            {
                EFMonkey.Schedule schedule = new EFMonkey.Schedule();

                schedule.Date = Convert.ToDateTime(date);
                schedule.Name = text;
                schedule.isDone = false;

                EFMonkey.User user = new EFMonkey.User();
                user = context.User.FirstOrDefault(u => u.Id == userId);
                schedule.User = user;

                context.Schedule.Add(schedule);
                
                context.SaveChanges();

                return schedule.Id;
            }
        }

        /// <summary>
        /// Holt alle Termine eines Tages
        /// </summary>
        /// <param name="userId">User-Id</param>
        /// <returns>DataTable mit Terminen</returns>
        public DataTable GetTodaysSchedules(int userId)
        {
            using (EFMonkeyContext context = new EFMonkey.EFMonkeyContext())
            {
                EFMonkey.User user = new EFMonkey.User();
                user = context.User.FirstOrDefault(u => u.Id == userId);
                DateTime today = DateTime.Now.Date;
                DataTable table = new DataTable();

                if(user != null){

                    IList<EFMonkey.Schedule> schedules = context.Schedule.Where(s => s.Date.Day.ToString() == today.Day.ToString()
                                                                                    && s.Date.Month.ToString() == today.Month.ToString()
                                                                                    && s.Date.Year.ToString() == today.Year.ToString()
                                                                                    && s.User.Id == user.Id).ToList();


                    if (schedules.Count > 0)
                    {
                        table.Columns.Add("name");
                        table.Columns.Add("id");
                        table.Columns.Add("done");

                        foreach (EFMonkey.Schedule scheduleEntry in schedules)
                        {
                            DataRow row = table.NewRow();
                            row["name"] = scheduleEntry.Name + " (" + scheduleEntry.Date.ToString("HH:mm") + ")";
                            row["id"] = scheduleEntry.Id;
                            row["done"] = scheduleEntry.isDone;
                            table.Rows.Add(row);
                            table.AcceptChanges();
                        }
                    }
                }
                return table;
            }
        }

        /// <summary>
        /// Aktualisiert die Erledigung eines Termins
        /// </summary>
        /// <param name="scheduleId">Termin Id</param>
        /// <param name="isChecked">neuer Zustand</param>
        public void UpdateSchedule(int scheduleId, bool isChecked)
        {
            using (EFMonkeyContext context = new EFMonkey.EFMonkeyContext())
            {
                EFMonkey.Schedule schedule = context.Schedule.FirstOrDefault(s => s.Id == scheduleId);
                schedule.isDone = isChecked;

                context.SaveChanges();
            }
        }
    }
}
