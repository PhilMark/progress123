﻿using EFMonkey;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/// <summary>
/// Implementation des Businesslogik
/// </summary>
namespace Administration
{
    /// <summary>
    /// Businesslogik für Quiz
    /// </summary>
    public class Quiz
    {
        /// <summary>
        /// Fügt eine Frage mit optionalem Bild pro Nutzer ein
        /// </summary>
        /// <param name="userId">User-Id</param>
        /// <param name="question">Fragestellung</param>
        /// <param name="answer1">Antwort1</param>
        /// <param name="answer2">Antwort2</param>
        /// <param name="answer3">Antwort3</param>
        /// <param name="answer4">Antwort4</param>
        /// <param name="correctAnswer">Id der korrekten Antwort</param>
        /// <param name="documentId">optionales Bild</param>
        public void InsertQuiz(int userId, string question, string answer1, string answer2, string answer3, string answer4, string correctAnswer, int? documentId)
        {   
            using (EFMonkeyContext context = new EFMonkey.EFMonkeyContext())
            {
                EFMonkey.User user = new EFMonkey.User();
                user = context.User.FirstOrDefault(u => u.Id == userId);

                EFMonkey.Quiz quiz = new EFMonkey.Quiz();

                quiz.User = user;

                quiz.Question = question;
                quiz.CorrectAnswer = correctAnswer;
                quiz.Answer1 = answer1;
                quiz.Answer2 = answer2;
                quiz.Answer3 = answer3;
                quiz.Answer4 = answer4;

                if (documentId.HasValue)
                {
                    EFMonkey.Document document = context.Document.FirstOrDefault(d => d.Id == documentId);
                    quiz.Document = document;
                }

                context.Quiz.Add(quiz);
                context.SaveChanges();
            }
        }

        /// <summary>
        /// Liefert ein Array mit Werten für eine Quizfrage (random)
        /// </summary>
        /// <param name="userId">User-Id</param>
        /// <returns>Array mit Quiz Inhalt</returns>
        public string[] GetQuestion(int userId)
        {
            using (EFMonkeyContext context = new EFMonkey.EFMonkeyContext())
            {
                List<EFMonkey.Quiz> quizes = context.Quiz.Where(q => q.User.Id == userId).ToList();
                int count = quizes.Count();

                if (count > 0)
                {
                    Random r = new Random();
                    int rInt = r.Next(count);

                    EFMonkey.Quiz quiz = quizes.ElementAt(rInt);

                    return new String[] { quiz.Question, quiz.Answer1, quiz.Answer2, quiz.Answer3, quiz.Answer4, quiz.CorrectAnswer, quiz.Document != null ? quiz.Document.Url + quiz.Document.Name : "" };
                }
                return new String[] { };
            }
        }
    }
}
