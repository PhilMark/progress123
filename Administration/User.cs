﻿using EFMonkey;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/// <summary>
/// Implementation des Businesslogik
/// </summary>
namespace Administration
{
    /// <summary>
    /// Der <see cref="Administration"/>-Namespace hält Klassen für die Implementation der Businesslogik
    /// </summary>
    [System.Runtime.CompilerServices.CompilerGenerated]
    class NamespaceDoc
    {
    }

    /// <summary>
    /// Businesslogik für Nutzer
    /// </summary>
    public class User
    {
        /// <summary>
        /// Setzt den Mailtext für einen User
        /// </summary>
        /// <param name="userId">User-Id</param>
        /// <param name="mailText">Bodytext</param>
        public void SetUserMailtextById(int userId, string mailText)
        {
            using (EFMonkeyContext context = new EFMonkey.EFMonkeyContext())
            {   
                EFMonkey.User user = context.User.FirstOrDefault(u => u.Id == userId);
                user.ContactText = mailText;
                context.SaveChanges();
            }
        }

        /// <summary>
        /// Holt den Mailtext für einen User
        /// </summary>
        /// <param name="userId">User-Id</param>
        /// <returns>Gibt den String des Textes zurück</returns>
        public string GetUserMailtextById(int userId)
        {
            using (EFMonkeyContext context = new EFMonkey.EFMonkeyContext())
            {
                EFMonkey.User user = context.User.FirstOrDefault(u => u.Id == userId);
                return user.ContactText;
            }
        }

        /// <summary>
        /// Holt den Namen des ersten Users
        /// </summary>
        /// <returns>string des Namen</returns>
        public string GetFirstUserName()
        {
            using (EFMonkeyContext context = new EFMonkey.EFMonkeyContext())
            {
                if (context.User.Count() > 0)
                {
                    EFMonkey.User user = context.User.FirstOrDefault();
                    return user.FirstName + " " + user.LastName;
                }
                return null;
            }
        }

        /// <summary>
        /// Holt den Namen eines bestimmten Users
        /// </summary>
        /// <param name="userId">User-Id</param>
        /// <returns>string des Namen</returns>
        public string GetUserNameById(int userId)
        {
            using (EFMonkeyContext context = new EFMonkey.EFMonkeyContext())
            {
                if (context.User.Count() > 0)
                {
                    EFMonkey.User user = context.User.FirstOrDefault(u => u.Id == userId);
                    return user.FirstName + " " + user.LastName;
                }
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId">User-Id</param>
        /// <returns>Mailtext</returns>
        public string GetUserContactTextById(int userId)
        {
            using (EFMonkeyContext context = new EFMonkey.EFMonkeyContext())
            {
                if (context.User.Count() > 0)
                {
                    EFMonkey.User user = context.User.FirstOrDefault(u => u.Id == userId);
                    return user.ContactText;
                }
                return null;
            }
        }

        /// <summary>
        /// Füght einen neien User unter einem Admin ein
        /// </summary>
        /// <param name="firstname">Vorname</param>
        /// <param name="lastname">Nachname</param>
        /// <param name="adminId">Admin-Id</param>
        /// <param name="userTypeId">User-Type (ToDo)</param>
        /// <returns>gibt die Id des neuen Users zurück</returns>
        public int InsertUser(string firstname, string lastname, int adminId, int userTypeId)
        {
            using (EFMonkeyContext context = new EFMonkey.EFMonkeyContext())
            {
                string loginName = firstname.ToLower() + lastname.ToLower();
                string password = lastname.ToLower();
                if (context.Login.Where(l => l.LoginName == loginName).Count() > 0)
                {
                    return -1;
                }

                Login loginEntity = new Login();
                loginEntity.LoginName = loginName;
                loginEntity.Password = password;

                EFMonkey.User user = new EFMonkey.User();
                user.FirstName = firstname;
                user.LastName = lastname;

                loginEntity.User = user;
                context.User.Add(user);
                context.Login.Add(loginEntity);

                UserType usertype = context.UserType.First(ut => ut.Id == 1);

                user.UserType = usertype;

                context.SaveChanges();

                return user.Id;
            }
        }

        /// <summary>
        /// Liefert einen Table mit den Usern zurück
        /// </summary>
        /// <returns>Einen Table mit allen Usern</returns>
        public DataTable GetUsers()
        {
            using (EFMonkeyContext context = new EFMonkey.EFMonkeyContext())
            {
                IList<EFMonkey.User> users = context.User.ToList();
                DataTable table = new DataTable();
                if (users.Count > 0)
                {
                    table.Columns.Add("name");
                    table.Columns.Add("id");

                    foreach (EFMonkey.User user in users)
                    {
                        DataRow row = table.NewRow();
                        row["name"] = user.FirstName + " " + user.LastName;
                        row["id"] = user.Id;
                        table.Rows.Add(row);
                        table.AcceptChanges();
                    }
                }
                return table;
            }
        }
    }
}
