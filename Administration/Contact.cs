﻿using EFMonkey;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Implementation des Businesslogik
/// </summary>
namespace Administration
{
    /// <summary>
    /// Businesslogik für Kontakte
    /// </summary>
    public class Contact
    {
        /// <summary>
        /// Einen Kontakt anlegen
        /// </summary>
        /// <param name="userId">ID des Users</param>
        /// <param name="name">Name des Users</param>
        /// <param name="adress">Adress-String des Users</param>
        /// <param name="documentId">optionale Document-Id für ein Bild des Users</param>
        /// <returns>Gibt Id des neuen Kontaktes zurück</returns>
        public int SaveContact(int userId, string name, string adress, int? documentId)
        {
            using (EFMonkeyContext context = new EFMonkey.EFMonkeyContext())
            {
                EFMonkey.Contact contact = new EFMonkey.Contact();

                contact.Mailadress = adress;
                contact.Name = name;

                EFMonkey.User user = new EFMonkey.User();
                user = context.User.FirstOrDefault(u => u.Id == userId);
                contact.User = user;

                if (documentId.HasValue) {
                    EFMonkey.Document document = context.Document.FirstOrDefault(d => d.Id == documentId);
                    contact.Document = document;
                }

                context.Contact.Add(contact);
                context.SaveChanges();

                return contact.Id;
            }
        }

        /// <summary>
        /// Gint einen DataTable von Kontakten für einen User zurück
        /// </summary>
        /// <param name="userId">User-Id</param>
        /// <returns>Gibt einen DataTable mit den benötigten Daten zurück</returns>
        public DataTable GetContacts(int userId)
        {
            using (EFMonkeyContext context = new EFMonkey.EFMonkeyContext())
            {
                EFMonkey.Contact contact = new EFMonkey.Contact();
                IList <EFMonkey.Contact> contacts = context.Contact.Where(c => c.User.Id == userId).ToList();

                DataTable table = new DataTable();
                if (contacts.Count > 0)
                {
                    table.Columns.Add("name");
                    table.Columns.Add("mail");
                    table.Columns.Add("url");

                    foreach (EFMonkey.Contact contactEntry in contacts)
                    {
                        DataRow row = table.NewRow();
                        row["name"] = contactEntry.Name;
                        row["mail"] = contactEntry.Mailadress;

                        // Hole Profilbild zu Kontakt
                        if (contactEntry.Document != null)
                        {
                            EFMonkey.Document document = context.Document.FirstOrDefault(d => d.Id == contactEntry.Document.Id);
                            row["url"] = document.Url + document.Name;
                        }
                        table.Rows.Add(row);
                        table.AcceptChanges();
                    }
                }
                return table;
            }
        }
    }
}
